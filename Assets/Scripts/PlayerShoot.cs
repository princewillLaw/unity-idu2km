﻿using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    float cooldown;
    float MAX_COOLDOWN = 5f;
    public GameObject bulletPrefab,BackGround;
    GameObject bulletGO;

    private void Start()
    {
        cooldown = MAX_COOLDOWN;
    }
    // Update is called once per frame
    void Update()
    {
        cooldown -= Time.deltaTime;
        if (cooldown > 0f)
            return;

            shootBullet();
    }

    void shootBullet() {
        bulletGO = Instantiate(bulletPrefab, BackGround.transform);
        bulletGO.transform.position = gameObject.transform.GetChild(0).position;
        bulletGO.GetComponent<BulletMovement>().spawner = gameObject;
        cooldown = MAX_COOLDOWN;
    }
}
