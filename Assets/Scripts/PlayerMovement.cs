﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    float speed = 400f;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("left")) {
            gameObject.GetComponent<RectTransform>().Translate(-Vector3.right * speed * Time.deltaTime);
        }

        if (Input.GetKey("right"))
        {
            gameObject.GetComponent<RectTransform>().Translate(Vector3.right * speed * Time.deltaTime);
        }

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name.Contains("Boundary"))
        {
            Destroy(gameObject);
            print("Game Over");
            PlayerPrefs.SetInt("red_score", PlayerPrefs.GetInt("red_score") + 1);
        }

    }
}
