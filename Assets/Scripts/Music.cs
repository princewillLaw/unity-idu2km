﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Music : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
        SceneManager.LoadScene("GameplayScene", LoadSceneMode.Single);
    }
}
