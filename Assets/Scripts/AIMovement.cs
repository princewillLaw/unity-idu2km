﻿using UnityEngine;

public class AIMovement : MonoBehaviour
{
    int direction = 0;
    float switchtimer = 0f;
    float MAX_SWITCH_TIMER = 3f;
    float speed = 400f;

    // Update is called once per frame
    void Update()
    {
        switchtimer -= Time.deltaTime;

        if (switchtimer < 0f)
        {
            //switch direction
            direction = (int)Random.Range(-2f, 2f);
            //reset switcher
            switchtimer = Random.Range(0f, MAX_SWITCH_TIMER);
        }
        //0 direction means stand still, -1 direction means go left, 1 direction means go right////////////////
        gameObject.GetComponent<RectTransform>().Translate(direction * Vector3.right * speed * Time.deltaTime);
       
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name.Contains("Boundary"))
        {
            //suppose to destroy him
            direction *= -1;
        }

    }
}
