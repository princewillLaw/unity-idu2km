﻿using UnityEngine;

public class LineSpawner : MonoBehaviour
{
    public GameObject linePrefab, Background;
    public GameObject[] leftLocations;
    public GameObject[] rightLocations;
    GameObject lineGO;

    float MIN_TIMER = 0f;
    float MAX_TIMER = 3f;
    float timer;

    private void Start()
    {
        timer = MIN_TIMER;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0f) {
            RandomReposition();
            

            timer = Random.Range(MIN_TIMER,MAX_TIMER);
        }
    }

    void RandomReposition() {
        int horizontalPos = (int)Random.Range(0f, 5f);
        switch ((int)Random.Range(0f, 2f)) {
            case 0:
                transform.position = leftLocations[horizontalPos].transform.position;
                lineGO = Instantiate(linePrefab,Background.transform);
                lineGO.GetComponent<LineMovement>().direction = -1;
                break;
            case 1:
                transform.position = rightLocations[horizontalPos].transform.position;
                lineGO = Instantiate(linePrefab,Background.transform);
                lineGO.GetComponent<LineMovement>().direction = 1;
                break;
        }
        lineGO.transform.position = transform.position;
    }
}
