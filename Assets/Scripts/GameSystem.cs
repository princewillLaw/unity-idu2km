﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameSystem : MonoBehaviour
{
    //AiMovement does not punish for wall touch
    //
    int redScore=0, blueScore=0;
    public Text redScoreText, blueScoreText;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("red_score"))
        {
            redScore = PlayerPrefs.GetInt("red_score");
        }
        else {
            PlayerPrefs.SetInt("red_score",0);
        }
        if (PlayerPrefs.HasKey("blue_score"))
        {
            blueScore = PlayerPrefs.GetInt("blue_score");
        }
        else{
            PlayerPrefs.SetInt("blue_score", 0);
        }
        //reset everything if someone reached 10
        if (PlayerPrefs.GetInt("red_score") >= 10 || PlayerPrefs.GetInt("blue_score") >= 10)
        {
            PlayerPrefs.SetInt("red_score", 0);
            PlayerPrefs.SetInt("blue_score", 0);
            redScore = 0;
            blueScore = 0;
        }

        redScoreText.text = "" + redScore;
        blueScoreText.text = "" + blueScore;
    }

    // Update is called once per frame
    void Update()
    {
        if (redScore != PlayerPrefs.GetInt("red_score") || blueScore != PlayerPrefs.GetInt("blue_score")) {
            SceneManager.LoadScene("GameplayScene", LoadSceneMode.Single);
        }

        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }
}
