﻿using UnityEngine;

public class LineMovement : MonoBehaviour
{

    public int direction = 0;
    float speed = 350f;

    // Update is called once per frame
    void Update()
    {
        if (direction == 0)
            return;

        switch (direction) {
            case -1:
                //move right
                gameObject.GetComponent<RectTransform>().Translate(Vector3.right * speed * Time.deltaTime);
                break;

            case 1:
                //move left
                gameObject.GetComponent<RectTransform>().Translate(-Vector3.right * speed * Time.deltaTime);
                break;
        }
        Destroy(gameObject, 10f);
    }
}
