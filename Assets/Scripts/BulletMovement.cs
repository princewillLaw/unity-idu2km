﻿using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    public GameObject spawner;
    float speed = 500f;
    int horizontalDirection = 0, verticalDirection = 0;
    bool spawnerSet = false;

    // Update is called once per frame
    void Update()
    {
        if (spawner == null)
            return;

        if (spawner.name == "PlayerBlue" && !spawnerSet) {
            verticalDirection = 1;
            spawnerSet = true;
        }

        if (spawner.name == "PlayerRed" && !spawnerSet) {
            verticalDirection = -1;
            spawnerSet = true;
        }

        gameObject.GetComponent<RectTransform>().Translate(verticalDirection * Vector3.up * speed * Time.deltaTime);
        gameObject.GetComponent<RectTransform>().Translate(horizontalDirection * Vector3.right * speed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (spawner == null)
            return;
        
        if (other.gameObject.name.Contains("Line"))
        {
            verticalDirection *= -1;
            horizontalDirection = other.gameObject.GetComponent<LineMovement>().direction * -1;
        }
        if (other.gameObject.name=="BoundaryT" || other.gameObject.name == "BoundaryB")
        {
            verticalDirection *= -1;
        }

        if (other.gameObject.name == "BoundaryR" || other.gameObject.name == "BoundaryL")
        {
            horizontalDirection *= -1;
        }

        if (other.gameObject.name == "PlayerBlue")
        {
            Destroy(other.gameObject);
            PlayerPrefs.SetInt("red_score", PlayerPrefs.GetInt("red_score") + 1);
        }

        if (other.gameObject.name == "PlayerRed")
        {
            Destroy(other.gameObject);
            PlayerPrefs.SetInt("blue_score", PlayerPrefs.GetInt("blue_score") + 1);
        }

    }
}
